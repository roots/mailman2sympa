#!/usr/bin/env python2
#! -*- coding: utf-8 -*-

import codecs
import os.path
import smtplib
import string
import textwrap
from email.mime.text import MIMEText

TEMPLATEDIR="./"
MAILSERVER="localhost"
MAILPORT="25"

def sendMailFromTemplate(template, to, subject, params, sender = 'root@fs.lmu.de'):
    if not os.path.isfile(TEMPLATEDIR + template):
        raise Exception('Template %s not found'%template)

    with codecs.open(TEMPLATEDIR + template, 'r', encoding='utf8') as f:
        t = string.Template(f.read())

    t = u'\n'.join(map(textwrap.fill, t.substitute(params).split(u'\n')))

    msg = MIMEText(t, "plain", "utf-8")
    msg['Subject'] = subject
    msg['From'] = sender
    msg['To'] = to

    smtp = smtplib.SMTP(MAILSERVER, MAILPORT)
    smtp.sendmail(sender, [to], msg.as_string().encode('utf-8'))
    smtp.quit()

import argparse
parser = argparse.ArgumentParser(description='notify owners about list movement')
parser.add_argument('input')
args = parser.parse_args()

v = {}
execfile(args.input, v)
for owner in v['owner']:
	sendMailFromTemplate('owner.template', owner, "Umzug der Mailingliste " + v['real_name'], v)

