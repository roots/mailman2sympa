#!/bin/zsh
# wrapper around sendmail to send commands to sympa

[ -f /etc/sympa/$1/robot.conf ]  || { echo "Robot does not exist" ; exit 1 }

TO="sympa@$1"

# sender must be a listmaster
FROM=${2:-listmaster@domain.tld}

exec sendmail -f $FROM $TO
