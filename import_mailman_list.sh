#!/bin/zsh

LIST=$1
ROBOT=${2:-fs.lmu.de}
NEWLIST=${3:-$LIST}
ARCHIVE=/srv/mailman/archives/private/$LIST.mbox/$LIST.mbox

[[ -n "$LIST" ]] || { echo "list may not be empty" ; exit 1 }
[[ -f "/etc/sympa/$ROBOT/robot.conf" ]] || { echo "robot does not exist" ; exit 1 }

echo "importing $LIST as $NEWLIST@$ROBOT"
read -q "REPLY?is this correct? [yN] "
[[ "$REPLY" = "y" ]] || exit

set -e
set -x

INPUT=$(mktemp)
config_list -o $INPUT $LIST
sed -e s/\ \'/\ u\'/ -e s/\ \"/\ u\"/ -i $INPUT
echo "new_listname = u'$NEWLIST'" >> $INPUT
echo "robot = u'$ROBOT'" >> $INPUT
awk '/^wwsympa_url/ { print "url = u\""$2"\""; }' < /etc/sympa/$ROBOT/robot.conf >> $INPUT

XML=$(mktemp)
./gen_config.py $INPUT $XML
chmod 644 $XML
/usr/lib/sympa/bin/sympa.pl --create_list --robot $ROBOT --input_file $XML
rm $XML

#import archive
if [[ -f "$ARCHIVE" ]] ; then
	TMP=$(TMPDIR=/srv mktemp)
	cp /srv/mailman/archives/private/$LIST.mbox/$LIST.mbox $TMP
	chmod 644 $TMP
	./import_mbox.sh $TMP $NEWLIST $ROBOT
	rm $TMP
fi

# add members
list_members -f $LIST | tr -d '"\\' | sed -e 's:\(.*\) <\(.*\)>:\2 \1:' -e "s:^:QUIET ADD $NEWLIST :" | ./send_command.sh $ROBOT

# sends a mail to every digest user, thus disabled
# set digest members to digest mode
#for digest in "$(list_members -d $LIST)" ; do
#  echo "QUIET SET $LIST DIGEST" | send_command.sh fs.lmu.de $digest
#done

rmlist $LIST

./notify_owner.py $INPUT
rm $INPUT
