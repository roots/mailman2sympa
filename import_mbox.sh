#!/bin/zsh

# import_mbox
# List archive import
# from Unix mbox format
# to Sympa's Web Archive (MHonArc)
#
# Based on Sympa contribs and tools
# Not too much sexy, as it won't be useful after
# initial conversion (during Sympa setup phase)
#
# Needs a mbox file and a Sympa listname
# Copies (just in case ;-) the mbox file
# under the archives directory of the list
#
# Written by E.Eyer - ESRF - 25 Oct 06
# Mostly rewritten by Hendrik - 11 Aug 14

# Configuration variables
SYMPA_USER=sympa:sympa
SYMPA_ROOT=/srv/sympa
MBOX_BACKUP=import.mbox

set -e

# Syntax check
SYNTAX="$0 archive.mbox listname domain"
MBOX_FILE="$1"
LIST_NAME="$2"
SYMPA_DOMAIN="$3"
[ ! -f "$MBOX_FILE" ] && echo "Missing or invalid archive file!" && echo "$SYNTAX" && exit 1
[ ! -d "$SYMPA_ROOT/list_data/$SYMPA_DOMAIN/$LIST_NAME" ] && echo "Missing or invalid list name!" && echo "$SYNTAX" && exit 1

# Create archives directory if not yet done
mkdir -p "$SYMPA_ROOT/list_data/$SYMPA_DOMAIN/$LIST_NAME/archives"

# Check directory is empty (prevents dialogs...)
COUNT=`ls $SYMPA_ROOT/list_data/$SYMPA_DOMAIN/$LIST_NAME/archives | wc -l`
(( COUNT > 0 )) && echo "Sorry, Sympa text archives directory not empty - I stop!" && exit 2

set -e
set -x

# Copy mbox file
echo "Step 1: copying input file to expl/$LIST_NAME/archives/$MBOX_BACKUP"
cp -v "$MBOX_FILE" "$SYMPA_ROOT/list_data/$SYMPA_DOMAIN/$LIST_NAME/archives/$MBOX_BACKUP"
pushd "$SYMPA_ROOT/list_data/$SYMPA_DOMAIN/$LIST_NAME/archives"

# Convert to Sympa text archives
echo "Step 2: converting $MBOX_BACKUP into .log files"
mbox2sympa.pl $MBOX_BACKUP
rm $MBOX_BACKUP

chown -R $SYMPA_USER "$SYMPA_ROOT/list_data/$SYMPA_DOMAIN/$LIST_NAME/archives"
chmod -R g=rX,o= "$SYMPA_ROOT/list_data/$SYMPA_DOMAIN/$LIST_NAME/archives"

# Convert to Sympa web archives input files
echo "Step 3: converting .log files into arc/${LIST_NAME}\@${SYMPA_DOMAIN}/\*/arctxt files"
su sympa -s /bin/bash -c "/usr/share/sympa/bin/arc2webarc.pl $LIST_NAME $SYMPA_DOMAIN" || true

# Generate the HTML archives
echo "Step 4: triggering web archives regeneration"
#su sympa -s /bin/bash -c "touch /var/spool/sympa/outgoing/.rebuild.$LIST_NAME@$SYMPA_DOMAIN"

# That's it!
echo "Job done - web archives should be ready in a few minutes"
echo "You may monitor the Sympa log for errors..."
exit 0
