#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import argparse
import xml.etree.ElementTree as ET

parser = argparse.ArgumentParser(description='import mailman list to sympa')
parser.add_argument('input')
parser.add_argument('output')
args = parser.parse_args()

old_vars = {}
execfile(args.input, old_vars)

xml = ET.Element("list")

ET.SubElement(xml, "listname").text = old_vars['new_listname']
ET.SubElement(xml, "status").text = 'open'
ET.SubElement(xml, "type").text = 'import'
# subject is required in sympa
if old_vars['description']:
	ET.SubElement(xml, "subject").text = old_vars['description']
else:
	ET.SubElement(xml, "subject").text = old_vars['real_name']
ET.SubElement(xml, "custom_subject").text = old_vars['subject_prefix'].strip().replace('[', '').replace(']', '')
if old_vars['advertised']:
	ET.SubElement(xml, "visibility").text = 'noconceal'
else:
	ET.SubElement(xml, "visibility").text = 'conceal'

if old_vars['subscribe_policy'] == 1:
	ET.SubElement(xml, "subscribe").text = 'auth'
else:
	ET.SubElement(xml, "subscribe").text = 'owner'

if old_vars['archive']:
	if old_vars['archive_private']:
		ET.SubElement(xml, "archive").text = 'private'
	else:
		ET.SubElement(xml, "archive").text = 'public'

if old_vars['private_roster']:
	ET.SubElement(xml, 'user_visibility').text = 'conceal'
else:
	ET.SubElement(xml, 'user_visibility').text = 'noconceal'

if old_vars['reply_goes_to_list'] == 0:
	ET.SubElement(xml, 'reply_to').text = 'sender'
elif old_vars['reply_goes_to_list'] == 1:
	ET.SubElement(xml, 'reply_to').text = 'list'
elif old_vars['reply_goes_to_list'] == 2:
	if old_vars['reply_to_address'] == old_vars['new_listname'] + '@' + old_vars['robot']:
		ET.SubElement(xml, 'reply_to').text = 'list'
	else:
		ET.SubElement(xml, 'reply_to').text = 'other_email'
		ET.SubElement(xml, 'reply_to_address').text = old_vars['reply_to_address']

if old_vars['first_strip_reply_to']:
	ET.SubElement(xml, 'strip_reply_to').text = 'forced'
else:
	ET.SubElement(xml, 'strip_reply_to').text = 'respect'

# send
if old_vars['generic_nonmember_action'] == 0: # accept
	ET.SubElement(xml, 'send').text = 'public'
elif old_vars['generic_nonmember_action'] >= 1: # hold, reject, discard
	if old_vars['default_member_moderation']:
		ET.SubElement(xml, 'send').text = 'editorkey'
	else:
		ET.SubElement(xml, 'send').text = 'privateoreditorkey'

# privateoreditorkey
# public
# private

for owner in old_vars['owner']:
	o = ET.SubElement(xml, "owner", attrib={'multiple':'1'})
	ET.SubElement(o, "email").text = owner

for mod in old_vars['moderator']:
	o = ET.SubElement(xml, "editor", attrib={'multiple':'1'})
	ET.SubElement(o, "email").text = mod

tree = ET.ElementTree(xml)
tree.write(args.output, encoding='utf-8', xml_declaration=True)
